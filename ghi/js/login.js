window.addEventListener('DOMContentLoaded', () => {
  // get nav-link elements that have d-none in their class name
  var navLinks = document.getElementsByClassName('nav-link d-none');
  console.log(navLinks);
  
  const form = document.getElementById('login-form');
  form.addEventListener('submit', async event => {
    event.preventDefault();
    const fetchOptions = {
      method: 'post',
      body: new FormData(form),
      include: 'https://user:password@example.com/',
    };
    const url = 'http://localhost:8000/login/';
    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      // Show nav links by removing d-none
      for(let i=0;i<navLinks.length;i++){
        navLinks[i].classList.remove('d-none');
      }
      window.location.href = '/';
    } else {
      console.error(response);
    }
  });
});
