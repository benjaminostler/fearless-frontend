window.addEventListener('DOMContentLoaded', async () => {
    var selectTag = document.getElementById('conference');
    var loadingIcon = document.getElementById('loading-conference-spinner');
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('conference');
        for (let conference of data.conferences) {
            const newOption = document.createElement('option')
            newOption.value = conference.href;
            newOption.innerHTML = conference.name;
            selectTag.appendChild(newOption);
        }
      // Here, add the 'd-none' class to the loading icon
      loadingIcon.classList.add('d-none');
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none');
    }
    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      // get form data
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
      // send data to server
      const attendeeUrl = 'http://localhost:8001/api/attendees/';
      const fetchConfig = {
      method: "post",
      body: json,
      headers: {
          'Content-Type': 'application/json',
      },
      };
      const response = await fetch(attendeeUrl, fetchConfig);
      if (response.ok) {
          formTag.reset();
          const attendeeForm = document.getElementById("create-attendee-form");
          const successMessage = document.getElementById("success-message");
          attendeeForm.classList.add('d-none');
          successMessage.classList.remove('d-none');
          const newAttendee = await response.json();
      }
    });

});
