function createCard(name, description, pictureUrl, location, starts, ends){
  return `
  <div class="card mb-3 shadow">
    <img src="${pictureUrl}" class="card-img-top" />
    <div class="card-body">
      <h5 class="card-title">${name}</h5>
      <h6 class="card-subtitle mb-1 text-muted">${location}</h6>
      <p class="card-text">${description}</p>
    </div>
    <div class="card-footer text-muted">
      <small class="text-muted">${starts} - ${ends}</small>
    </div>
  </div>
  `
}


function formatDate(dateString){
  const date = new Date(dateString);
  const month = date.getMonth()+1;
  const day = date.getDay()+1;
  const year = date.getFullYear();
  return (`${month}/${day}/${year}`);
}


window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';
  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error("Response not ok");
    } else {
      const data = await response.json();
      let columnIndex = 1;
      for(let conference of data.conferences){
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name;
          const starts = formatDate(details.conference.starts);
          const ends = formatDate(details.conference.ends);
          const cardHtml = createCard(name, description, pictureUrl, location, starts, ends);
          const column = document.querySelector(`#column${columnIndex}`);
          column.innerHTML += cardHtml;
          columnIndex++;
          if(columnIndex > 3){
            columnIndex = 1;
          }
        }
      }
    }
  } catch (e) {
    console.error(e);
    showAlert("Alert Message Triggered!");
  }

});
