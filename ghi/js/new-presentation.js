window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('conference');
        for (let conference of data.conferences) {
            const newOption = document.createElement('option')
            newOption.value = conference.href;
            newOption.innerHTML = conference.name;
            selectTag.appendChild(newOption);
        }
    }

    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        // get form data
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        // send data to server
        const presentationUrl = `http://localhost:8000${conference.value}presentations/`;
        const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
        }

    });
});
